<?php namespace LuizReginaldo\ActivityStream;

use Illuminate\Support\ServiceProvider;

class ActivityStreamServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        $this->publishConfiguration();
        $this->publishMigrations();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }

    /**
     * Publish configuration file.
     */
    private function publishConfiguration()
    {
        $this->publishes([__DIR__ . '/../../resources/config/activity_stream.php' => config_path('activity_stream.php')], 'config');
        $this->mergeConfigFrom(__DIR__ . '/../../resources/config/activity_stream.php', 'activity_stream');
    }

    /**
     * Publish migration file.
     */
    private function publishMigrations()
    {
        $this->publishes([__DIR__ . '/../../resources/migrations/' => base_path('database/migrations')], 'migrations');
    }
}